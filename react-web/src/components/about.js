import React,{ Component } from 'react';
import '../css/about.css';
import img1 from '../assets/Juice/cucuberry.jpg';
import img2 from '../assets/Juice/cucuapple.jpg';
import img3 from '../assets/Juice/030115_Carrot-Juice.jpg';
import img4 from '../assets/download.png';
import img5 from '../assets/Juice/beetroot.png';
import img6 from '../assets/Juice/g1.png';
import img7 from '../assets/Juice/3.jpg';
import img8 from '../assets/Juice/Pineapple-Ginger-Mojitos-5.jpg';
import img9 from '../assets/Juice/xgWAWfNcQUW08c7H8GB1_018-watermelon-juice.jpg';




class About extends Component{
	render(){
		return (
				<div className="col-lg-12 col-md-12 col-sm-12 back_color hidden-xs">
				  <div className="col-lg-12 col-md-12 col-sm-12 pdd_2">
				    
				    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 padd_tp_55">
				     
				      <div className="col-lg-9 col-md-9 col-sm-9 pdd_1" id="left_panel">
				        <div className="col-lg-12 col-md-12 col-sm-12">
				          <div className="col-lg-12 col-md-12 col-sm-12 normal_fontt bred_name_sty">
				            <a href="../" className="clrr"><span>Home</span></a>
				            <span ><i className="fa fa-angle-right" aria-hidden="true"></i> Juice</span>
				            
				          </div>
				          <div className="col-lg-6 col-md-6 col-sm-6 normal_fontt">
				            <div className="col-lg-4 col-md-4 col-sm-4">
				              <input type="radio" name="text"/> All
				            </div>
				            <div className="col-lg-4 col-md-4 col-sm-4">
				              <input type="radio" name="text"/> Veg
				            </div>
				            <div className="col-lg-4 col-md-4 col-sm-4">
				              <input type="radio" name="text"/> Non-veg
				            </div>
				          </div>
				          <div className="col-lg-6 col-md-6 col-sm-6 normal_fontt">
				            <div className="col-lg-8 col-md-8 col-sm-8 pad_right_search">
				              <input type="text" name="Search" placeholder="Search Menu" className="search_box "/>
				              <i className="fa fa-search seacrh_icon" aria-hidden="true"></i>
				            </div>
				            <div className="col-lg-4 col-md-4 col-sm-4 pad_right_search">Sort By Price
				              <img src="assets/image/sort.png" className="sort_img"/>
				            </div>
				          </div>
				        </div>
				        <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 padd_07">
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 item_border backgrnd_color bydefault_size">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 ">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img2} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">Cucuberry</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 120</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 veg_view_like">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 23</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 26</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl bydefault_size">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src="../assets/Juice/cucuapple.jpg" className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">Cucuapple</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 80</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 20</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 22</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img3} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">Caped crusader</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 60</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 21</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 29</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				        </div>
				        <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 padd_07">
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img5} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">Superbeet</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 120</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 20</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 29</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img6} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">G1</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 100</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 21</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 20</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img7} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">G3</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 90</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 20</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 27</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				        </div>
				        <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 padd_07">
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img8} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">Pineapple RAW</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 50</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 30</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 31</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				          <div className="col-lg-4 col-md-4 col-sm-4 gap_item">
				            <div className="col-lg-12 col-md-12 col-sm-12 gap_itom">
				              <div className="col-lg-12 col-md-12 col-sm-12 item_border backgrnd_color">
				                <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <img src={img9} className="item_imagee img-responsive"/>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-8 col-md-8 col-sm-8 ite_Name">Leon</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 item_price">₹ 20</div>
				                    </div>
				                    <div className="col-lg-12 col-md-12 col-sm-12 pdd_0">
				                      <div className="col-lg-6 col-md-6 col-sm-6 pdd_0 food_namwe">
				                        <img src={img4} className="veg_icon"/> Delight Food</div>
				                      <div className="col-lg-2 col-md-2 col-sm-2 pdd_0 txt_right food_namwe">
				                        <i className="fa fa-eye" aria-hidden="true"></i> 10</div>
				                      <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 tr_Right food_namwe">
				                        <i className="fa fa-heart" aria-hidden="true"></i> 16</div>
				                    </div>
				                  </div>
				                  <div className="col-lg-12 col-md-12 col-sm-12 pdd_0 pad_totl">
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0">
				                      <div className="star_rating">4.2</div>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 add_box" >
				                      <span className="decrease_btn" >-</span>
				                      <span>0</span>
				                      <span className="increment_btn">+</span>
				                    </div>
				                    <div className="col-lg-4 col-md-4 col-sm-4 pdd_0 txt_right">
				                      <button className="add_butn">Add</button>
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
				          </div>
				          
				        </div>
				      </div>
				      <div className="col-lg-3 col-md-3 col-sm-3" id="right_panel">
				        <div className="col-lg-12 col-md-12 col-sm-12 order_outer">
				          <div className="col-lg-12 col-md-12 col-sm-12 order_itom">Your order</div>
				          <div className="col-lg-12 col-md-12 col-sm-12 set_addres">Set delivery address</div>
				          <div className="col-lg-12 col-md-12 col-sm-12 iteom_name">
				            <div className="col-lg-6 col-md-6 col-sm-6 pad_0">Roast Pork Chow</div>
				            <div className="col-lg-3 col-md-3 col-sm-3 itome_price">400</div>
				            <div className="col-lg-3 col-md-3 col-sm-3 itome_price">
				              <i className="fa fa-trash" aria-hidden="true"></i>
				            </div>
				          </div>
				          <div className="col-lg-12 col-md-12 col-sm-12 payment_box">
				            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
				              <div className="col-lg-6 col-md-6 col-sm-6 pad_right">Sub total</div>
				              <div className="col-lg-6 col-md-6 col-sm-6">100</div>
				            </div>
				            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
				              <div className="col-lg-6 col-md-6 col-sm-6 pad_right">Delivery Charge</div>
				              <div className="col-lg-6 col-md-6 col-sm-6">30</div>
				            </div>
				            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
				              <div className="col-lg-6 col-md-6 col-sm-6 pad_right">GST</div>
				              <div className="col-lg-6 col-md-6 col-sm-6">150</div>
				            </div>
				            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
				              <div className="col-lg-6 col-md-6 col-sm-6 pad_right">Total</div>
				              <div className="col-lg-6 col-md-6 col-sm-6">400</div>
				            </div>
				          </div>
				          <div className="col-lg-12 col-md-12 col-sm-12 check_div">
				            <button className="checkout_buttonn">Checkout</button>
				          </div>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>










			)
	}
}

export default About;