import React,{ Component } from 'react';
import  '../css/footer.css';
import immg from '../assets/image/Logo White.png'

class Footer extends Component{
	render(){
		return (
				<div className="col-lg-12 col-md-12 col-sm-12 hidden-xs footerBackground  ">
        <div className="footer_gap">
          <div className="col-md-3 col-sm-3 col-lg-3">
            <img src={immg} className="logo img_fitang"/>
          </div>
          <div className="col-md-3 col-sm-3 col-lg-3 footer_color normal_font">
            <div className="footer_header_menu">Quick Links</div>
            <div className="pad_22">Home</div>
            
            <div className="pad_22">Menu</div>
            
            <div className="pad_22">Contact</div>
          </div>
          <div className="col-md-3 col-sm-3 col-lg-3 footer_color normal_font">
            <div className="footer_header_menu">Menu</div>
            <div className="pad_22">Cold Press Juice</div>
            <div className="pad_22">Sandwiches</div>
            <div className="pad_22">Eggs to Order</div>
            <div className="pad_22">Side Eats</div>
            <div className="pad_22">Biryanis</div>
            <div className="pad_22">Salads</div>
            <div className="pad_22">Steaks</div>
            <div className="pad_22">Smoothies</div>
          </div>
          <div className="col-md-3 col-sm-3 col-lg-3 footer_color normal_font">
            <div className="footer_header_menu">Contact Info</div>
            <div className="pad_22">167 Science Center Drive,USA</div>
            <div className="pad_22">+987-654-321</div>
            <div className="pad_22">info@vegan.com</div>
            <div className="footer-icon">
              <a href="https://fb.me/letsoccasions" target="_blank">
                <img src="//letsoccasion.com/assets/images/home/footer/new/fb.png" className="img-width"/>
              </a>
              <a href="https://twitter.com/letsoccasion" target="_blank">
                <img src="//letsoccasion.com/assets/images/home/footer/new/twitter.png" className="img-width"/>
              </a>
              <a href="https://plus.google.com/u/0/b/110642296625803817676/110642296625803817676" target="_blank">
                <img src="//letsoccasion.com/assets/images/home/footer/new/google Plus.png" className="img-width"/>
              </a>
              <a href="https://www.instagram.com/letsoccasion/" target="_blank">
                <img src="//letsoccasion.com/assets/images/home/footer/new/insagram.png" className="img-width"/>
              </a>
              <a href="https://www.pinterest.com/letsoccasion//" target="_blank">
                <img src="//letsoccasion.com/assets/images/home/footer/new/pintrest.png" className="img-width"/>
              </a>
              <a href="https://www.linkedin.com/company/let%27s-occasion" target="_blank">
                <img src="//letsoccasion.com/assets/images/home/footer/new/linkedin.png" className="img-width"/>
              </a>
            </div>
          </div>
        </div>
      </div>
				
			)
	}
}

export default Footer;