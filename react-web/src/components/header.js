import React,{ Component } from 'react';
import img from '../assets/image/Fiatng.png';
import '../css/header.css';
import axios from 'axios';
import image from '../assets/close-icon-white-border.png'


import * as FontAwesome from 'react-icons/lib/fa'


class Popup extends React.Component {
	constructor(props){
		super(props);
		this.loginSubmit = this.loginSubmit.bind(this);
				
		this.state = {
          username: '',
          password: '',
          showPopupLogin: false
          
        };
        this.stateSignup = {
         
          password: '',
          firstName:'',
          lastName:'',
          mobile:'',
          email:'',
          showPopupLogin: false
        };
	}
	
	loginSubmit(){
		console.log('inn')
	}
	onChange = (e) => {
        
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
      }
      onSignupChange = (e) => {
      	console.log(e.target.value);
        
        const stateSignup = this.stateSignup
        stateSignup[e.target.name] = e.target.value;
        this.setState(stateSignup);
      }

      onSubmit = (e) => {
      
        e.preventDefault();
        
        const { username,password } = this.state;

        axios.post('http://fitang.in:8080/faapi/customer/publiclogin'+'?username='+username+'&password='+password,
         { username,password })
          .then((result) => {
          	let res=result.data
          	if (res.success==1) {
          		console.log(res.data);
          		showPopupLogin: false

          	}
          	
            
          });
      }
      onSignUp = (e) => {
      	console.log(this.stateSignup);
      
        e.preventDefault();
        
        let formData = new FormData();
		formData.append('data', JSON.stringify(this.stateSignup));
		

        axios.post('http://fitang.in:8080/faapi/customer/publicsave',
         formData)
          .then((result) => {
          	let res=result.data
          	console.log(res.data);
            
          });
      }
  render() {
  	const {  username,password } = this.state;
  	const {  firstName,lastName,mobile,email } = this.stateSignup;
    return (
      <div className='popup'>
        <div className='popup_inner'>
        <div  >
        	<img src={image} className="close_img" onClick={this.props.closePopup}/>
        </div>
        	<div className="col-lg-12 col-md-12 col-sm-12 padd_0">
        		<div className="col-lg-12 col-md-12 col-sm-12 padd_0">
        			<div className="col-lg-7 col-md-7 col-sm-7 outer_box">
        				<div className="col-lg-12 col-md-12 col-sm-12 signup_title">Signup</div>
        				<form onSubmit={this.onSignUp}>
	        				<div className="col-lg-12 col-md-12 col-sm-12 input_divv">
	        					<input type="text" name="firstName" placeholder="Enter first name" className=" input_boxx" value={firstName} onChange={this.onSignupChange}/>
	        				</div>
	        				<div className="col-lg-12 col-md-12 col-sm-12 input_divv">
	        					<input type="text" name="lastName" placeholder="Enter last name" className=" input_boxx" value={lastName} onChange={this.onSignupChange}/>
	        				</div>
	        				<div className="col-lg-12 col-md-12 col-sm-12 input_divv">
	        					<input type="text" name="email" placeholder="Enter email name" className=" input_boxx" value={email} onChange={this.onSignupChange}/>
	        				</div>
	        				<div className="col-lg-12 col-md-12 col-sm-12 input_divv">
	        					<input type="text" name="mobile" placeholder="Enter mobile number" className=" input_boxx" value={mobile} onChange={this.onSignupChange}/>
	        				</div>
	        				<div className="col-lg-12 col-md-12 col-sm-12 input_divv">
	        					<input type="text" name="password" placeholder="Enter password name" className=" input_boxx" value={password} onChange={this.onSignupChange}/>
	        				</div>
	        				<div className="col-lg-12 col-md-12 col-sm-12 input_divv">
	        					<button className="sign_up" type="submit">Signup</button>
	        				</div>
        				</form>
        			</div>
        			<div className="col-lg-5 col-md-5 col-sm-5 padd_0 lob_box">
        				<div className="col-lg-12 col-md-12 col-sm-12 loog_inn">Login</div>
        				<form onSubmit={this.onSubmit}>
	        				<div className="col-lg-12 col-md-12 col-sm-12 log_box">
				            	<div className="log_title">Email/Mobile number</div>
					            <input type="text" className="log_input" name="username" placeholder="Email/mobile number" value={username} onChange={this.onChange} />
					            <div className="log_title">Password</div>
					            <input type="text" className="log_input" name="password" placeholder="Password" value={password} onChange={this.onChange} />
					            <div className="submit_div"> 
					            	<button type="submit" className="submit_btom">Submit</button>
					            </div>
					            
				            </div>
			            </form>
        			</div>
        		</div>
        	</div>
        </div>
      </div>
    );
  }
}

class Header extends Component{

	constructor() {
    super();
    this.state = {
      showPopupLogin: false
      
    };
  }
  togglePopup() {
    this.setState({
      showPopupLogin: !this.state.showPopupLogin
      
    });
  }


	render(){
		return (
				<div className="col-lg-12 col-md-12 col-sm-12 padd_1">
					<div className="col-lg-3 col-md-3 col-sm-3 Padd_0">
						<img src={img} alt="title" className='img-responsive full_widthh'/>
					</div>
					<div className="col-lg-5 col-md-5 col-sm-5 Padd_0 App2">
						<div className="font padd_20_15 col-lg-4 col-md-4 col-sm-4 txt_right">Home</div>
						<div className="font padd_20_15 col-lg-4 col-md-4 col-sm-4">Category</div>
						<div className="font padd_20_15 col-lg-4 col-md-4 col-sm-4 txt_left">Search</div>
					</div>
					<div className="col-lg-4 col-md-4 col-sm-4 Padd_0">
						<div className="col-lg-6 col-md-6 col-sm-6 font padd_20_15 login_box">
							<button className="login_btnn" onClick={this.togglePopup.bind(this)}>
				          Login
				        </button>
				        {this.state.showPopupLogin ? 
				          <Popup
				            text='Login'
				            closePopup={this.togglePopup.bind(this)}
				          />
				          : null
				        }
				        
						</div>
						<div className="col-lg-6 col-md-6 col-sm-6 font padd_20_15">Signup</div>
					</div>
				</div>
				
			)
	}
}


export default Header;