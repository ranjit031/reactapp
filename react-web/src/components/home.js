import React,{ Component } from 'react';
import  '../css/home.css';
import TableList from './table';
import img1 from '../assets/Juice/3.jpg';
import img2 from '../assets/Sandwiches/4.jpg';
import img3 from '../assets/smoothies/2.jpg';
import img4 from '../assets/Nonveggrilled/1.jpg';
import img5 from '../assets/salads/4.jpg';
import img6 from '../assets/Veg/1.jpg';


class Home extends Component{

	juiceClickHandler=()=>{
		console.log('inn');
		this.setState( () => {
        	this.props.history.push('about');
    	});
	}
  


	render(){
		return (
     // <div> <TableList /></div>
				<div className="item_category col-lg-12 col-md-12 col-sm-12 pad_0 hidden-xs">
  <div className="inner_category col-lg-12 col-md-12 col-sm-12 pad_0">
    <div className="category_title">Category</div>
    <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" onClick={this.juiceClickHandler}>
          <div><img src={img1} title="Juice" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Cold Press Juice</span>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img2} title="Sandwiches" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Sandwiches</span>
               
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img3} title="eggorder" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Eggs to Order</span>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img3} title="sideeats" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Side Eats</span>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img4} title="biryanis" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Biryanis</span>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img5} title="Salads" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Salads</span>
               
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img6} title="Steaks" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Steaks</span>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-3 pad_1 ">
        <div className="onhover" >
          <div><img src={img6} title="Smoothies" alt="Juice" className="ful_width"/></div>
          <div className="effect">
            <div className="col-lg-12 col-md-12 col-sm-12 pad_0">
              <div className="col-lg-12 col-md-12 col-sm-12 mar_20">
                <span className="item__name">Smoothies</span>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
				
			)
	}
}

export default Home;