import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import About from './about';
import Home from './home';
import Header from './header';
import Footer from './footer';

const header =(props)=>{
  
  
  return (
     <Router>
      <div className="col-lg-12 col-md-12 col-sm-12 Padd_0">
       <div className="col-lg-12 col-md-12 col-sm-12 Padd_0">
          <Header/>
       </div>
       <div className="col-lg-12 col-md-12 col-sm-12 Padd_0">
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          
       </div>
       <div className="col-lg-12 col-md-12 col-sm-12 Padd_0">
          <Footer />
       </div>
      </div>
    </Router>
    )
}

export default header;