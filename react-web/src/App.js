import React, { Component } from 'react';

import './App.css';
import Main from './components/main'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="col-lg-12 col-md-12 col-sm-12 Padd_0">
            <Main />
        </div>
      </div>
    );
  }
}

export default App;
